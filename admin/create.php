<?php

session_start();
require('../app/app.php');

$config['users']->ensure_user_is_authenticated();

if (is_get()) {
  $categories = $config['provider']->get_categories();

  if ($categories == false) {
    admin_view('not_found');
    die();
  }
}


if (is_post()) {
  $name = $_POST['name'];
  $address = $_POST['address'];
  $postal_code = $_POST['postal_code'];
  $city = $_POST['city'];
  $phone = $_POST['phone'];
  $category_id = $_POST['category_id'];

  if(empty($name) || empty($address) || empty($postal_code) || empty($city) || empty($category_id)) {
    echo "Niste popunili sva obavezna polja";
  } else {
    $config['provider']->add_partner($name, $address, $postal_code, $city, $phone, $category_id);
    redirect('index.php');
  }
}



admin_view('create', array(
  'categories' => $categories
));
