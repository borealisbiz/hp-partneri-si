<?php

session_start();
require('../app/app.php');

$config['users']->ensure_user_is_authenticated();

if (is_get()) {
  $key = $_GET['id'];

  if (empty($key)) {
    view('not_found');
    die();
  }

  $items = $config['provider']->get_partner($key);

  if ($items == false) {
    admin_view('not_found');
    die();
  }

  $categories = $config['provider']->get_categories();

  if ($categories == false) {
    admin_view('not_found');
    die();
  }


}

if (is_post()) {
  $id = $_POST['id'];
  $name = $_POST['name'];
  $address = $_POST['address'];
  $postal_code = $_POST['postal_code'];
  $city = $_POST['city'];
  $phone = $_POST['phone'];
  $category_id = $_POST['category_id'];


  $config['provider']->edit_partner($id, $name, $address, $postal_code, $city, $phone, $category_id);
  redirect('index.php');
}

admin_view('edit', array(
  'item' => $items,
  'categories' => $categories
));
