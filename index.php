<?php

require('app/app.php');

if(empty($_GET['category'])){
  $category = 1;
}else{
  $category = $_GET['category'];
}

$items = $config['provider']->get_partners($category);
$categories = $config['provider']->get_categories();
$msg = '';

view('index', array(
  'items' => $items,
  'category_id' => $category,
  'categories' => $categories
));
