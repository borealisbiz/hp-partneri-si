<?php

class Users {

  public $email;
  public $pass;

  private $hostname;
  private $dbname;
  private $dbport;
  private $dbuser;
  private $dbpass;

  function __construct($_hostname = '', $_dbname = '', $_dbport = '', $_dbuser = '', $_dbpass = '') {
    $this->hostname = $_hostname;
    $this->dbname = $_dbname;
    $this->dbport = $_dbport;
    $this->dbuser = $_dbuser;
    $this->dbpass = $_dbpass;
    }

  private function connect() {
    $con_str = 'mysql:dbname=' . $this->dbname . ';host=' . $this->hostname . ';port=' . $this->dbport . "'";
    $user = $this->dbuser;
    $password = $this->dbpass;

    try {
        return new PDO($con_str, $user, $password);
    } catch (PDOException $e) {
        echo "Could not connect to database";
    }
    }

  public function get_users($email) {
    $db = $this->connect();

    $sql = 'SELECT * FROM users WHERE email = :email';

    $result = $db->prepare($sql);
    $result->execute(['email' => $email]);

    $data = $result->fetchAll(PDO::FETCH_CLASS, 'Users');

    if(count($data) == 0){
     return null;
    } else {
     return $data[0];
    }
  }

  public function authenticate_user($email, $pass) {
    $user = $this->get_users($email);

    if ($user == null) {
      return array(
        'success' => false,
        'message' => "The provided credentials didn't work"
      );
    }

    if (password_verify($_POST['pass'], $user->pass)) {

      $_SESSION['email'] = $user->email;
      return array(
        'success' => true
      );
    } else {
      return array(
        'success' => false,
        'message' => "The provided credentials didn't work"
      );
    }
  }

  public function is_user_authenticated() {
    return isset($_SESSION['email']);
  }

  public function ensure_user_is_authenticated() {
    if (!$this->is_user_authenticated()) {
      redirect('../login.php');
    }
  }

}
