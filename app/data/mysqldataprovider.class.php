<?php

require('partners.class.php');
require('categories.class.php');

class MySqlDataProvider {
  private $hostname;
  private $dbname;
  private $dbport;
  private $dbuser;
  private $dbpass;

  function __construct($_hostname, $_dbname, $_dbport, $_dbuser, $_dbpass) {
    $this->hostname = $_hostname;
    $this->dbname = $_dbname;
    $this->dbport = $_dbport;
    $this->dbuser = $_dbuser;
    $this->dbpass = $_dbpass;
    }

  private function connect() {
    $con_str = 'mysql:dbname=' . $this->dbname . ';host=' . $this->hostname . ';port=' . $this->dbport . "'";
    $user = $this->dbuser;
    $password = $this->dbpass;

    try {
        return new PDO($con_str, $user, $password);
    } catch (PDOException $e) {
        return null;
    }
    }

  public function get_users($email, $pass) {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }

    $sql = 'SELECT * FROM users';

    $result = $db->query($sql);

    $data = $result->fetchAll(PDO::FETCH_CLASS, 'Users');

    $result = null;
    $db = null;

    return $data;
  }



  public function get_categories() {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }

    $records = array();

    $result = $db->query("SET NAMES utf8");

    $sql = 'SELECT * FROM categories';

    $result = $db->query($sql);

    $data = $result->fetchAll(PDO::FETCH_CLASS, 'Categories');

    $result = null;
    $db = null;

    return $data;

    }


  public function get_partner($id) {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }


    $sql = 'SELECT * FROM partners WHERE id = :id';

    $result = $db->query("SET NAMES utf8");
    $result = $db->prepare($sql);
    $result->execute(['id' => $id]);

    $data = $result->fetchAll(PDO::FETCH_CLASS, 'Partners');

    if (empty($data)) {
        return false;
    }

    $result = null;
    $db = null;

    return $data[0];
  }

  public function get_partners($category) {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }

    $records = array();

    $result = $db->query("SET NAMES utf8");

    $sql = 'SELECT * FROM partners';
    if(!empty($category)){
      $sql = $sql . ' WHERE category_id = ' . $category;
    }
    $result = $db->query($sql);


    $data = $result->fetchAll(PDO::FETCH_CLASS, 'Partners');

    $result = null;
    $db = null;

    return $data;
  }


  public function add_partner($name, $address, $postal_code, $city, $phone, $category_id) {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }

    $sql = 'INSERT INTO partners (name, address, postal_code, city, phone, category_id) VALUES (:name, :address, :postal_code, :city, :phone, :category_id)';

    $result = $db->prepare($sql);

    $result->execute([':name' => $name, ':address' => $address, ':postal_code' => $postal_code, ':city' => $city, ':phone' => $phone, ':category_id' => $category_id]);

    $result = null;
    $db =  null;
  }

  public function edit_partner($id, $name, $address, $postal_code, $city, $phone, $category_id) {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }

    $sql = 'UPDATE partners SET name = :name, address = :address, postal_code = :postal_code, city = :city, phone = :phone, category_id = :category_id WHERE id = :id';

    $result = $db->prepare($sql);

    $args = [':name' => $name, ':address' => $address, ':postal_code' => $postal_code, ':city' => $city, ':phone' => $phone, ':category_id' => $category_id, ':id' => $id];

    $result->execute($args);

    $result = null;
    $db = null;

  }

  public function delete_partner($id) {
    $db = $this->connect();

    if ($db === null) {
        echo "Spajanje na bazu nije uspjelo";
    }

    $sql = 'DELETE FROM partners WHERE id = :id';

    $result = $db->prepare($sql);

    $result->execute([':id' => $id]);

    $result = null;
    $db = null;
  }

}
