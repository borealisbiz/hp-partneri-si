  <div class="row">
      <div class="col-lg-12 text-center">
          <h1 class="mt-5">Uredi partnera</h1>
      </div>
  </div>

  <div class="row align-items-center justify-content-center">
  <div class="col-md-4">
  <form action="" method="POST" accept-charset="ISO-8859-1">
    <input type="hidden" name="id" value="<?= $model['item']->id ?>" />
    <div class="form-group">
      <label for="name">Naziv *</label>
      <input class="form-control" type="text" name="name" id="name" value="<?= $model['item']->name ?>" />
    </div>
    <div class="form-group">
      <label for="address">Adresa *</label>
      <input class="form-control" type="text" name="address" id="address" value="<?= $model['item']->address ?>" />
    </div>
    <div class="form-group">
      <label for="postal_code">Poštanski broj *</label>
    <input class="form-control" type="text" name="postal_code" id="postal_code" value="<?= $model['item']->postal_code ?>" />
    </div>
    <div class="form-group">
      <label for="city">Grad *</label>
    <input class="form-control" type="text" name="city" id="city" value="<?= $model['item']->city ?>" />
    </div>
    <div class="form-group">
      <label for="phone">Telefon</label>
      <input class="form-control" type="text" name="phone" id="phone" value="<?= $model['item']->phone ?>" />
    </div>
    <div class="form-group">
      <label for="category_id">Kategorija *</label>
      <select class="form-control" name="category_id" id="category_id">
        <?php foreach ($model["categories"] as $category) : ?>
        <option value="<?= $category->category_id; ?>"><?= $category->category; ?></option>
      <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group">
      <input class="form-control" type="submit" value="Ažuriraj partnera">
    </div>
  </form>
  </div>
</div>
</div>
