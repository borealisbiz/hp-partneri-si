<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="shortcut icon" type="image/ico" href="http://www.hppartneri.hr/favicon.ico" />
		<title>HP® | Popis partnera | Slovenia</title>
		<style type="text/css" title="currentStyle">
			@import "../css/demo_page.css";
			@import "../css/demo_table.css";
			@import "../css/main.css";
			@import "../css/bootstrap.min.css";
		</style>
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="http://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
		<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
		$('.dataTable').dataTable( {
				"oLanguage": {
						"sLengthMenu": "Prikaži _MENU_ partnera po stranici",
						"sZeroRecords": "Ništa nije pronađeno",
			"sSearch": "Pretraži",
						"sInfo": "Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera",
						"sInfoEmpty": "Prikazano 0 do 0 od ukupno _TOTAL_ partnera",
						"sInfoFiltered": "filtrirano iz _MAX ukupnih rezultata",
					"oPaginate": {
				"sFirst":    "Prva",
				"sPrevious": "Nazad",
				"sNext":     "Naprijed",
				"sLast":     "Zadnja"
		}
				}
		} );
		} );
		</script>
	</head>

	<body id="dt_example">
	  <div id="container">
	    <h1><img src="../img/hp-logo.jpg" width="84" height="84" alt="HP" id="hpImg">Partneri</h1>
	<div id="usual1" class="usual">
	<ul>
	  <li><a href="index.php">Prikaz svih partnera</a></li>
	  <li><a href="create.php">Unos novog partnera</a></li>
		<li><a href="../logout.php">Odjava</a></li>
	</ul>

  <?php require("$name.view.php"); ?>

  <script type="text/javascript">
    $("#usual1 ul").idTabs();
  </script>

  			<div class="clear"></div>

  		</div>
  	</body>
  </html>
