	<body id="dt_example">

		<div id="container">

			<h1><img src="./img/hp-logo.jpg" width="84" height="84" alt="HP" id="hpImg"><?php echo ($msg['Partneri']); ?></h1>

	<div id="usual1" class="usual">
  <ul>

		<?php foreach ($model["categories"] as $category) :

				$selected_class = '';
				if ($model["category_id"] == $category->category_id) {
					$selected_class = "selected";
				} ?>

    <li><a href="<?= getBaseUrl(); ?>?category=<?= $category->category_id; ?>" class="<?= $selected_class ?>"><?= $category->category; ?></a></li>
		
	<?php endforeach; ?>

  </ul>
  <div id="tab1">
  <div id="demo_trident">
<table cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="allan">
	<thead>
		<tr class="tableHeader">
			<th><?php echo ($msg['Partner']); ?></th>
			<th><?php echo ($msg['Adresa']); ?></th>
			<th><?php echo ($msg['Grad']); ?></th>
			<th><?php echo ($msg['Broj telefona']); ?></th>
		</tr>
	</thead>
	<tbody>

		<?php	foreach ($model["items"] as $partner) : ?>

 <tr class="gradeA">
	 <td><?= $partner->name ?></td>
	 <td><?= $partner->address ?></td>
	 <td><?= $partner->postal_code ?> <?= $partner->city ?></td>
	 <td><?= $partner->phone ?></td>
 </tr>

<?php endforeach; ?>

	</table>
			</div></div>
</div>
