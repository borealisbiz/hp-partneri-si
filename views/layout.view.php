<?php require('app/language/si.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="shortcut icon" type="image/ico" href="http://www.hppartneri.hr/favicon.ico" />
		<title>HP <?php echo($msg['Popis partnera | Hrvatska']); ?></title>
		<style type="text/css" title="currentStyle">
		@import "css/demo_page.css";
		@import "css/demo_table.css";
		@import "css/main.css";
		@import "css/bootstrap.min.css";
		</style>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="http://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
		<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
$('.dataTable').dataTable( {
        "oLanguage": {
            "sLengthMenu": "<?php echo ($msg['Prikaži _MENU_ partnera po stranici']); ?>",
            "sZeroRecords": "<?php echo ($msg['Ništa nije pronađeno']); ?>",
			"sSearch": "<?php echo ($msg['Pretraži']); ?>",
            "sInfo": "<?php echo ($msg['Prikazano _START_ do _END_ od ukupno _TOTAL_ partnera']); ?>",
            "sInfoEmpty": "<?php echo ($msg['Prikazano 0 do 0 od ukupno _TOTAL_ partnera']); ?>",
            "sInfoFiltered": "(<?php echo ($msg['filtrirano iz _MAX ukupnih rezultata']); ?>)",
			    "oPaginate": {
        "sFirst":    "Prva",
        "sPrevious": "<?php echo ($msg['Nazad']); ?>",
        "sNext":     "<?php echo ($msg['Naprijed']); ?>",
        "sLast":     "Zadnja"
    }
        }
    } );
} );
		</script>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3807659-25', 'hppartneri.hr');
  ga('send', 'pageview');

</script>
	</head>

  <?php require("$name.view.php"); ?>

  <script type="text/javascript">
    $("#usual1 ul").idTabs();
  </script>

  			<div class="clear"></div>

  			<div id="footer" class="clear" style="text-align:center;">
  				<p>© Copyright <?php echo date("Y"); ?> HP Development Company, L.P.</p>
  			</div>
  		</div>
  	</body>
  </html>
